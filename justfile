ri := "registry.gitlab.com/connected-technologies/docker-images"

build-ruby-node-22:
    docker build \
        --platform linux/amd64 \
        -f ruby-node/22/Dockerfile \
        -t {{ri}}/ruby-node:3.4.1-22 \
        .

build-ruby-node-8:
    docker build \
        --platform linux/amd64 \
        -f ruby-node/8/Dockerfile \
        -t {{ri}}/ruby-node:2.6.3-8 \
        .

build-node-14:
    docker build \
        --platform linux/amd64 \
        -f node/14/Dockerfile \
        -t {{ri}}/node:14 \
        .

run-ruby-node-8:
    docker run \
        -it --rm \
        -t {{ri}}/ruby-node:2.6.3-8 \
        /bin/bash

run-ruby-node-22:
    docker run \
        -it --rm \
        -t {{ri}}/ruby-node:3.4.1-22 \
        /bin/bash

run-ruby-node-latest:
    docker run \
        -it --rm \
        -t {{ri}}/ruby-node:latest \
        /bin/bash
